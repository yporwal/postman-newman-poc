const newman = require('newman');
const fs = require("fs"), path = require("path");
const _ = require('lodash');
const minimist = require('minimist');
const async = require('async');

let args = minimist(process.argv.slice(2), {
    alias: {
        c: 'collections',
        e: 'environment'
    },
    default: {
        c: ""
    },
});

getAllCollections = function(){
    let dirPath = path.join(__dirname, "collections");
    let collections;
    collections = fs.readdirSync(dirPath);
    return collections;
}

updateResourcePath = function(collection){
    collection.item.map( _it => {
        if(typeof _it.item !="undefined"){
            console.log('folder found::', _it['name']);
            return updateResourcePath(_it);
        }else if(typeof _it['request'] != 'undefined' && _it['request'].method == "POST" && _it['request'].body.mode == "formdata"){
            console.log('request found::', _it['name'])
            return _it['request'].body.formdata.map(data => {
                if(data.type == "file"){
                    console.log('replacing file path..')
                    let lIndex = (data.src.lastIndexOf('/') >= 0 ) ? data.src.lastIndexOf('/') : data.src.lastIndexOf('\\');
                    data.src = path.join(__dirname ,'resources' , data.src.slice(lIndex+1, data.src.length));
                }
                return data
            });
        }
    });
}

proccessCollection = function (filepath){
    let collection = JSON.parse(fs.readFileSync(filepath));
    updateResourcePath(collection);
    fs.writeFileSync( filepath, JSON.stringify(collection));
}




run = function(){
    //console.log(args.colections)
    let collections = (args['collections'] == 'all') ? getAllCollections() : args['collections'].split(',');
    console.log(collections);
    async.eachSeries(collections, (file, next) => {
        proccessCollection( path.join(__dirname, 'collections', file));
        start(args['environment'], file, next);
    }, (err, result) => {
        console.error(err);
    })
}

start = function(environment, collection, callback){
   
    newman.run({
        collection: path.join(__dirname, 'collections', collection), 
        environment: path.join(__dirname, 'environment', environment),
        reporters: ['htmlextra','cli','junit'],
        reporter: {
            html: {
                export: path.join(__dirname, 'reports', collection.slice(0, collection.lastIndexOf('.')) + (new Date()).toISOString().replace(/[^\d]+/g, '_') + '.report.html'),
                template: path.join(__dirname, 'template', 'customTemplate_2.hbs'),
            },
            htmlextra: {
                export: path.join(__dirname, 'reports', collection.slice(0, collection.lastIndexOf('.')) + (new Date()).toISOString().replace(/[^\d]+/g, '_') + '.report.html'),
                //template: path.join(__dirname, 'template', 'customTemplate_2.hbs'),
            },
           junit:{
               export: path.join(__dirname, 'reports')
           }
        }
    },(err, summary) => {
        callback(err, summary);
    })
}


console.log('args:',args);
run();